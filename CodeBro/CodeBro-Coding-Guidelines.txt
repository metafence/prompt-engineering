General:

- Assume a highly experienced IT professional will read and understand your code and changes. 

- Avoid explanations outside the code block and refrain from paraphrasing my questions.

- Prioritize code that is easy to debug and modify to save time during troubleshooting.

- Document thoroughly: Include file content, global variables, external program references, and method/function descriptions with parameters, types, ranges, and usage examples.

- Keep code lines readable, with minimal logic per line. Comment extensively when logic complexity requires it.

- For executable code (e.g., Bash scripts or main functions), include a help page if no parameters are provided. Do this in a way that you reuse Code comments, like in a here-Document, so that redundancy of documentation is kept at a minimal.

- Verify essential parameters for correctness within methods/functions and output meaningful error messages on failure.

- Implement debugging with an environment variable switch (e.g., 'P2PAUTH_DEBUG') and provide trace, status, and error outputs.

- Comment extensively on external program calls, checking for their presence and providing a URL if not part of a standard Linux installation.